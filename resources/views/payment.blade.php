<!DOCTYPE html>
<!--
Template Name: Jovaphile
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html>
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->
<head>
<title>Ganit Class | Payment</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="<?php echo URL('/'); ?>/assets/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <img src = "<?php echo URL('/'); ?>/assets/images/logo/logo.jpeg">
  </header>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
      <li class="active"><a href="">Home</a></li>
      <!-- <li><a href="{{Route('register')}}">Registration</a></li> -->
      <li><a href="#">Free Sample Class Video</a></li>
      <li><a href="#">Phone/Whatsapp</a></li>
      <!-- <li><a href="{{Route('login')}}">Login</a></li> -->
      <li><a href="{{ route('logout') }}"
        onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
        </form>
      </li>
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>

<div class="wrapper row3">
  <main class="hoc container clear"> 
  <div class="col-md-8">
    <div class="card">
        <!-- <div class="card-header">{{ __('Register') }}</div> -->
            <div class="card-body">
                <form method="POST" action="{{ route('payment') }}">
                    @csrf
                    
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Paytm Mobile No') }}</label>
                        <div class="col-md-6">
                            <input id="mobile_no" type="text" class="form-control @error('mobile_no') is-invalid @enderror" name="mobile_no" required >
                            @error('mobile_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Pay') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clear"></div>
  </main>
</div>

<div>
  <div id="copyright" class="hoc clear"> 
    <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved - <a href="#">Domain Name</a></p>
    <p class="fl_right">Template by <a target="_blank" href="" title="Free Website Templates">OS Templates</a></p>
  </div>
</div>
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="<?php echo URL('/'); ?>/assets/layout/scripts/jquery.min.js"></script>
<script src="<?php echo URL('/'); ?>/assets/layout/scripts/jquery.backtotop.js"></script>
<script src="<?php echo URL('/'); ?>/assets/layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>