@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Exam
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{route('exam_add')}}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Upload Exam Pdf</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<!-- <div class = "row">
			<div class = "col-md-12">
				<form action = "{{route('video_list')}}" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">			
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div> -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Exam No</th>
					<th>Exam</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($exams as $exam) { ?>
			<tr>
				<td>{{ $exam->id}}</td>
				<td>{{ $exam->exam}}</td>
				<td>
					<i class="m-menu__link-icon fa fa-edit">
						<a href="">Edit</a>
					</i>&nbsp;&nbsp;
					<!-- <i class="m-menu__link-icon fa fa-trash-alt">
							<a href="">Delete</a>
					</i> -->
				</td>
				</tr>
			<?php } ?>							
			</tbody>
			
		</table>
	</div>
</div>
</div>
</div>
</div>     
@endsection
