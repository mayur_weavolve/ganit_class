@extends('layouts.main')

@section('content')
<div class = "row">
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
            <!-- <th> ID</th> -->
            <th>Exam Number</th>
            <th>Exam PDF</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($exams as $exam) { ?>
    <tr>
        <td>Exam {{ $exam->id}}</td>
        <td>{{ $exam->exam}}</td>
        <td><a href = "<?php echo url('/').'/exam_pdf/'.$exam->exam;?>" class="btn btn-primary" download>Download Exam Paper</a></td>
        </tr>
    <?php } ?>							
    </tbody>
    
</table>
</div>
@endsection