@extends('layouts.main')
@section('content')
            
            <div class="card-body">
            
                <form method="POST" action="{{ route('update_profile') }}">
                    @csrf
                    <input name="id" value="{{$users[0]->id}}" type="hidden" />
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-input-control" name="name" value="{{ $users[0]->name }}" required autocomplete="name" placeholder="Enter Name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                        <div class="col-md-6">
                            <input id="address" type="text" class="form-input-control" name="address" value="{{ $users[0]->address}}" placeholder="Enter Address" required autocomplete="current-address">

                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="mobile_no" class="col-md-4 col-form-label text-md-right">{{ __('Mobile Number') }}</label>

                        <div class="col-md-6">
                            <input id="mobile_no" type="text" class="form-input-control" name="mobile_no" value="{{ $users[0]->phone_no}}" placeholder="Enter Mobile number" required autocomplete="current-mobile_no">

                            @error('mobile_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="whatsapp_no" class="col-md-4 col-form-label text-md-right">{{ __('Whats app Number') }}</label>

                        <div class="col-md-6">
                            <input id="whatsapp_no" type="text" class="form-input-control" name="whatsapp_no" value="{{ $users[0]->whatsapp_no}}" placeholder="Enter What'sapp Number" required autocomplete="current-whatsapp_no">

                            @error('whatsapp_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update') }}
                            </button>

                           
                        </div>
                    </div>
                </form>
            </div>
@endsection        