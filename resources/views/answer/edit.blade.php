@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Edit Answer
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('answer_list') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>
					


	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
                        <?php foreach ($answers as $answer) { ?>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label"></label>
									<div class="col-xl-8 col-lg-8">
                                    <label class="col-xl-3 col-lg-3 col-form-label"><u><b>Question {{$answer->id}}</u></b></label>
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Answer:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="answer" class="form-control m-input" >
										<option value="">Select</option>
										<option value="a" <?php if( $answer->answer == 'a'){ echo 'selected'; } ?>>A</option>
										<option value="b" <?php if( $answer->answer == 'b'){ echo 'selected'; } ?>>B</option>
										<option value="c" <?php if( $answer->answer == 'c'){ echo 'selected'; } ?>>C</option>
										<option value="d" <?php if( $answer->answer == 'd'){ echo 'selected'; } ?>>D</option>											
									</select>
									@error('answer')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('answer') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
																
							</div>
                            <?php  } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
           
@endsection
