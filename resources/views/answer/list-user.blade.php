@extends('layouts.main')
@section('content')
<div class = "row">
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
<?php foreach ($exams as $exam) { ?>
<tr>
<td><b>Exam Paper</b></td>
<td><a href = "<?php echo url('/').'/exam_pdf/'.$exam->exam;?>" class="btn btn-primary" download>Download </a></td>
</tr>
<?php } ?>
</table>
	
<form method="post" class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="get" action="">
{{csrf_field() }}
    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
        <thead>
            <tr>
                <!-- <th> ID</th> -->
                <th>Question Number</th>
                <th>Answer</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($answers as $answer) { ?>
        <tr>
            <td>Question {{ $answer->id}}</td>
            <td>
                <select name="answer[{{ $answer->id}}]" class="form-control m-input" >
                    <option value="">Select</option>
                    <option value="a">A</option>
                    <option value="b">B</option>
                    <option value="c">C</option>
                    <option value="d">D</option>
                </select>
            </td>
            </tr>
            <?php } ?>		
        </tbody>
        
    </table>
    <div class="col-lg-8 m--align-right">
        <button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
            <span>
                <i class="la la-check"></i>
                    <span>Submit</span>
            </span>
        </button>
                                        
    </div>	
</form>
</div>

@endsection