@extends('layouts.main')
@section('content')
<div class = "row">
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
            <!-- <th> ID</th> -->
            <th>Question Number</th>
            <th>Your Answer</th>
            <th>Correct Answer</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($answers as $answer) { ?>
    <tr>
        <td>Question {{ $answer->id}}</td>
        <td>{{ $submitted[$answer->id] }}</td>
        <td>{{ $answer->answer}}</td>
        </tr>
    <?php } ?>							
    </tbody>
    
</table>
</div>

@endsection