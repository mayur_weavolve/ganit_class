<!DOCTYPE html>
<!--
Template Name: Jovaphile
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html>
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->
<head>
<title>Ganit Class</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="<?php echo URL('/'); ?>/assets/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>

.form-input-control{
    display: block !important;
    width: 100% !important;
    margin-bottom: 15px;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;"
}
.card-body{
    margin: 0px 100px 0px 100px;
}
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 10px;
}
</style>

</head>
<body id="top">
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <img style="width:300px" src = "<?php echo URL('/'); ?>/assets/images/logo/logo.jpg">
  </header>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <nav id="mainav" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul class="clear">
     <li><a href="">Home</a></li>
      <li><a href="{{route('semple')}}">Free Sample Class Video</a></li>
      
      <li><a href="tel:7359133626">Phone</a></li>
      <li><a href="https://api.whatsapp.com/send?phone=+917359133626">Whatsapp</a></li>
      @auth
      <li><a href="{{route('user_video')}}">Video</a></li>
      <!-- <li><a href="{{Route('exam_user')}}">Exam</a></li> -->
      <li><a href="{{Route('list_user')}}">Answer</a></li>
      <li><a href="{{Route('profile')}}">Profile</a></li>
      <li><a href="{{Route('logout')}}">Logout</a></li>
     
      <li><a href="{{Route('login')}}">Login</a></li>
      <li><a href="{{Route('register')}}">Registration</a></li>
      @endguest
      @if(Auth::user()->role == 'admin')
      <li><a href="http://ganitclass.com/user/list">Admin</a></li>
      @endif
      
    </ul>
    <!-- ################################################################################################ -->
  </nav>
</div>

<div class="wrapper row3">
  <main class="hoc container clear"> 
  <div class="col-md-8">
        <div class="card">
            
            @yield('content')
        </div>
    </div>
    <div class="clear"></div>
  </main>
</div>

<div>
  <div id="copyright" class="hoc clear"> 
    <p class="fl_left">Copyright &copy; 2020 - All Rights Reserved - <a href="#">ganitclass.com</a></p>
  </div>
</div>
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="<?php echo URL('/'); ?>/assets/layout/scripts/jquery.min.js"></script>
<script src="<?php echo URL('/'); ?>/assets/layout/scripts/jquery.backtotop.js"></script>
<script src="<?php echo URL('/'); ?>/assets/layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>
