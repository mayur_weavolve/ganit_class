@extends('layouts.main')

@section('content')
<div class = "row">
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
        <tr>
            <!-- <th> ID</th> -->
            <th>Title</th>
            <th>Description</th>
            <th>Youtube Link</th>
        </tr>
    </thead>
    <tbody>
    <!-- <?php foreach ($videos as $video) { ?> -->
    <tr>
        <td>{{ $video->name}}</td>
        <td>{{ $video->description}}</td>
        <td><a href = "{{route('view_video',[$video->id])}}" class = "btn btn-primary">View</a></td>
        </tr>
    <?php } ?>							
    </tbody>
    
</table>
</div>
@endsection