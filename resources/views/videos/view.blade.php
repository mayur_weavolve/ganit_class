@extends('layouts.main')
@section('content')
<div style="text-align: center;">
<?php foreach ($videos as $video) { ?>
<iframe style="width:100%" height="600px" src="{{$video->youtube_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>         
    <?php } ?>
</div>
@endsection
