@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Users
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<!-- <li class="m-portlet__nav-item">
					<a href="" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New User Add</span>
						</span>
					</a>
				</li> -->
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<div class = "col-md-12">
				<form action = "{{route('user_list')}}" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">			
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Name</th>
					<th>Birth Date</th>
					<th>Address</th>
					<th>Phone No</th>
					<th>Whatsapp No</th>
                    <th>status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user) { ?>
			<tr>
				<!-- <td> <?php echo $user->id ;?> </td> -->
				<td>{{ $user->name}}</td>
				<td>{{ $user->birth_date}}</td>
				<td>{{ $user->address}}</td>
				<td>{{ $user->phone_no}}</td>
				<td>{{ $user->whatsapp_no}}</td>
				<td>{{ $user->status}}</td>
				<td>
				
					<i class="m-menu__link-icon">
							<a href="{{route('user_delete',[$user->id])}}">{{ $user->status}}</a>
					</i>
				</td>
				</tr>
			<?php } ?>							
			</tbody>
			
		</table>
		{{$users->links()}}
	</div>
</div>
</div>
</div>
</div>     
@endsection
