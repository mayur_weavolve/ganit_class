@extends('layouts.main')
@section('content')
<style>
.card-body{
    margin: 0px 0px 0px 0px;
}
</style>

                <div class="card-body">
                   <table>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Birth Date</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Whats App Number</th>
                            <th>Payment Option</th>
                        </tr>
                        @if(count($users) == 0)
				            <tr><td colspan="3">No Data Found</td></tr>
			            @else
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td> {{ $user->birth_date }}</td>
                            <td>{{ $user->address}} </td>
                            <td>{{ $user->phone_no}}</td>
                            <td>{{ $user->whatsapp_no}}</td>
                            <td>{{ $user->payment_option}}</td>
                        </tr>
                       @endforeach
                       @endif
                    </table>
                </div>
      
@endsection
