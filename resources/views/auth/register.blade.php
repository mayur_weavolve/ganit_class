@extends('layouts.main')
@section('content')

<style>
    label{
         font-size: 18px;
    }
</style>
            <div class="card-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                    <input type="hidden" name="role" value="user" />
                        <label for="name">{{ __('Name') }}</label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-input-control" name="name" placeholder="Enter Name" required  >

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Birth Date') }} <small>(Password)</small></label>
                        <div class="col-md-6">
                            <input id="birth_date" type="text" class="form-input-control" placeholder="DDMMYYYY" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])(0[1-9]|1[012])[0-9]{4})" minlength="8" maxlength="8" name="birth_date" title="Enter valid date" required >
                            @error('birth_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                   
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Mobile no') }} <small>(User Id)</small></label>
                        <div class="col-md-6">
                            <input id="phone_no" type="tel" class="form-input-control" name="phone_no" minlength="10" maxlength="10" pattern="[0-9]{10}" title="Enter valid phone number" placeholder="Enter Mobile no" required >
                            @error('phone_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>
                        <div class="col-md-6">
                            <input id="address" type="text" class="form-input-control" name="address" required placeholder="Enter Address" />
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Payment Option') }}</label>

                        
                            <select class="form-input-control" name="payment_option"  required>
                                <option value="">Select Payment Option</option>
                                <option value="paytm">Net Banking</option>
                                <option value="paytm">Paytm/UPI/Credit & Debit Card</option>
                                <option value="Cash">Cash</option>
                                
                            </select>
    <div>If payment option selected Cash then contact by Phone.</div>
                            @error('payment_option')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Submit') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
@endsection