<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/mm', function () {
           echo mail("patelmit69@gmail.com", "New User Registration", "Hello Admin \n\nNew User registed with Name : "); //. $user->name );

    return view('we');
});
Route::get('', 'HomeController@index');
Route::get('semple', 'HomeController@semple')->name('semple');
//Route::get('payment', 'PaymentController@payment')->name('payment');
//Route::post('payment', 'PaymentController@save_payment');

Route::group(['middleware' => 'auth'], function(){
    
    Route::get('welcome','HomeController@index')->name('welcome');
    Route::get('/payment', 'PaymentController@payment')->name('payment');
    Route::post('/payment', 'PaymentController@save_payment');

    Route::get('user/list', 'AdminController@user_list')->name('user_list');
    Route::get('user/list','AdminController@serach')->name('user_list');
    Route::get('user/delete/{id}', 'AdminController@user_delete')->name('user_delete');

    Route::get('video/list', 'VideoController@list')->name('video_list');
    Route::get('video/list','VideoController@serach')->name('video_list');
    Route::get('videos/list','VideoController@user_video')->name('user_video');
    Route::get('video/view/{id}','VideoController@view_video')->name('view_video');
    Route::get('video/add', 'VideoController@add')->name('video_add');
    Route::post('video/add', 'VideoController@video_save');
    Route::get('video/edit/{id}', 'VideoController@edit')->name('video_edit');
    Route::post('video/edit/{id}', 'VideoController@video_editsave');
    Route::get('video/delete/{id}', 'VideoController@video_delete')->name('video_delete');

    Route::get('answers/list', 'AnswerController@list')->name('answer_list');
    Route::get('answer/list', 'AnswerController@list_user')->name('list_user');
    Route::post('answer/list', 'AnswerController@view_answer');
    Route::get('answer/edit/{id}', 'AnswerController@edit')->name('answer_edit');
    Route::post('answer/edit/{id}', 'AnswerController@answer_editsave');

    Route::get('exam/list', 'ExamController@list')->name('exam_list');
    Route::get('exam/lists', 'ExamController@list_user')->name('exam_user');
    Route::get('exam/add', 'ExamController@add')->name('exam_add');
    Route::post('exam/add', 'ExamController@exam_save');

    Route::get('/home', 'UserInfoController@index')->name('home');
    Route::get('/profile' ,'ProfileController@index')->name('profile');
    Route::post('/update/profile' ,'ProfileController@profile')->name('update_profile');
    Route::get('/payment/success' ,'PaymentController@thankyou');
    Route::get('/payment/fail' ,'PaymentController@fail');
    Route::get('/payment/info' ,'PaymentController@info');
    Route::get('/logout','Auth\LoginController@logout')->name('logout');

});

Auth::routes();


