<?php 

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Notifications\UserRegisteredNotification;

use Mail;

use PaytmWallet;
use Auth;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo ='/payment';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone_no' => ['required', 'unique:users'],
            'birth_date' => ['required'],
            'address' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
     
        return  User::create([
            'name' => $data['name'],
            'birth_date' => $data['birth_date'],
            'address' => $data['address'],
            'phone_no' => $data['phone_no'],
            'whatsapp_no' => $data['phone_no'],
            'email' => $data['phone_no'],
            'password' => Hash::make($data['birth_date']),
            'payment_option' => $data['payment_option'],
            'role' => "2",
            'status' => 'inactive',
            'is_deleted' =>'0',
            'role_type' =>'user',
        ]);
    return $user;

    }
    protected function registered($user) {
       
        mail("ganitclass10@gmail.com", "New User Registration", "Hello Admin \n\nNew User registed with Name : " . $user->name . " Phone Number : ". $user->phone_no );
        mail("patelmit69@gmail.com", "New User Registration", "Hello Admin \n\nNew User registed with Name : " . $user->name . " Phone Number : ". $user->phone_no );
        //$user->notify(new UserRegisteredNotification($user));
    }
}
