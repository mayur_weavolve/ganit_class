<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

use PaytmWallet;
use App\Payment;

class PaymentController extends Controller
{
   
    public function payment()
    {
        $user = Auth::user();
        if($user->payment_option == "paytm"){
          $payment = PaytmWallet::with('receive');
          $payment->prepare([
            'order' => $user->id,
            'user' => str_replace(" ", "_", $user->name),
            'mobile_number' => $user->email,
            'email' => 'student'. $user->id .'@ganitclass.com',
            'amount' => 1500,
            'callback_url' => url('api/payment/status')
          ]);
          return $payment->receive();
        }else{
          return redirect("/payment/info");
        }
    }
    public function save_payment(Request $request)
    {
        $this->validate($request, [
            'mobile_no' => 'required|numeric|digits:10',
        ]);
        $user = Auth::user();
        $input = $request->all();
        $input['user_id'] = $user->id;
        $input['order_id'] = $request->mobile_no.rand(1,100);
        $input['fee'] = 3;
        Payment::create($input);

        $payment = PaytmWallet::with('receive');
        $payment->prepare([
          'order' => $input['order_id'],
          'user' => 'mayur_agrawal',
          'mobile_number' => '9574622489',
          'email' => 'test@gmail.com',
          'amount' => $input['fee'],
          'callback_url' => url('api/payment/status/' )
        ]);
        return $payment->receive();
    }
    public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');

        $response = $transaction->response();
        $order_id = $transaction->getOrderId();
        $user = Auth::user();

        if($transaction->isSuccessful()){
            $user = Auth::user();
            $input = $request->all();
            $input['user_id'] = $user->id;
            $input['order_id'] = $user->mobile_no . rand(1,100);
            $input['fee'] = 1500;
            $input['status'] = 1;
            $input['transaction_id'] = $transaction->getTransactionId();
            Payment::create($input);
            redirect("/payment/success");

        }else if($transaction->isFailed()){
            Payment::where('order_id',$order_id)->update(['status'=>0, 'transaction_id'=>$transaction->getTransactionId()]);
            redirect("/payment/fail");
        }
    } 


    public function info ()
    {
        return view('payment.info');
    }
    public function fail ()
    {
        return view('payment.fail');
    }
    public function thankyou ()
    {
        return view('payment.success');
    }

}
