<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use VIPSoft\Unzip\Unzip;
use App\User;
use App\Category;
use Validator;

class BookController extends Controller
{
    public function book()
    {
        $teacher = Auth::user();
        $list = Book::paginate(5);
        $data = [];
        $data['books'] = $list;
        return view('book.list',$data);
    }
    public function add_book()
    {
        $category = Category::get();
        $data = [];
        $data['categories'] = $category;
        return view('book.add',$data);
    }
    public function save_book(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'category' => 'required',  
            'book_image' => 'required',  
            'book_upload' => 'required',  
           
        ]);
        if ($validator->fails()) {
            return redirect(route('add_book'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $teacher = Auth::user();
        $data = $request->all();
        $book = new Book();
        $book->teacher_id = $teacher->id;
        $book->category_id = $data['category'];
        $book->name = $data['name'];
        $book->status = 'active';

        $book->book_image = $request->file('book_image')->hashName('');
        $request->file('book_image')
        ->store('image', ['disk' => 'public']);

        $book->book_upload = $request->file('book_upload')->hashName('');
        $request->file('book_upload')
        ->store('book', ['disk' => 'public']);
        $book->save();
        // $unzipper  = new Unzip();
        // $filenames = $unzipper->extract('book', ['disk' => 'public']);

        // $path = public_path('/public/book/').$book->book_image;
        // \Zipper::make($Path)->extractTo('book', ['disk' => 'public']);
        return redirect('book'); 
        
    }
    public function book_detail($id)
    {
        $list = Book::where('category_id',$id)->paginate(5);
        $data = [];
        $data['books'] = $list;
        return view('book.detail',$data);
    }
    public function book_view($id)
    {
        $list = Book::where('id',$id)->get();
        $data = [];
        $data['books'] = $list;
        return view('book.book-view',$data);
    }
}
