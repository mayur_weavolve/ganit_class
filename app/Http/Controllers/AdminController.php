<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

use App\User;

class AdminController extends Controller
{
    public function user_list()
    {
        $user = Auth::user();
        $users = User::where('role_type','user')->orderBy('name')->paginate(10);
        $data=[];
        $data['users']=$users;
        return view('user.list',$data);
    }
    public function serach(Request $request)
    {
        $serach = $request->get('search');
        $list=User::where('name','like',"{$serach}%")->where('role_type','user')->orderBy('name')->paginate(10);
        $data=[];
        $data['users']=$list;
        return view('user.list',$data);
    }
    public function user_delete($id)
    {
        $user = Auth::user();
        $delete = User::find($id);
        if($delete->status == 'active')
        {
            $delete->is_deleted = '1';
            $delete->status = 'deactive';
            $delete->save();
        }
        else{
            $delete->is_deleted = '0';
            $delete->status = 'active';
            $delete->save();
        }
        
        return redirect('user/list');
    }
}
