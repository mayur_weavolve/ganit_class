<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

use App\Answer;
use App\Exam;

class AnswerController extends Controller
{
    public function list()
    {
        $user = Auth::user();
        $list = Answer::where('is_deleted','0')->get();
        $data = [];
        $data['answers'] = $list;
        return view('answer.list',$data);
    }
    public function list_user()
    {
        $user = Auth::user();
        $list = Answer::where('is_deleted','0')->get();
        $list1 = Exam::get();
        $data1 = [];
        $data = [];
        $data['answers'] = $list;
        $data1['exams'] = $list1;
        return view('answer.list-user',$data,$data1);
    }
    public function edit($id)
    {
        $user = Auth::user();
        $edit = Answer::where('id',$id)->get();
        $data = [];
        $data['answers'] = $edit;

        return view('answer.edit',$data);
    }
    public function answer_editsave($id,Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $edit = Answer::find($id);
        $edit->answer = $data['answer'];
        // $edit->is_deleted = '0';
        $edit->save();
        return redirect('answers/list');
    }
    public function view_answer(Request $request)
    {
        $user = Auth::user();
        $list = Answer::where('is_deleted','0')->get();
        $postData = $request->all();
        $data = [];
        $data['answers'] = $list;
        $data['submitted'] = $postData['answer'];
        return view('answer.answer-view',$data);
    }
}
