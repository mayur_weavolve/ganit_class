<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\User;
use Validator;

class TeacherController extends Controller
{
    public function student()
    {
        $teacher = Auth::user();
        $list=User::where('role_type',3)->where('is_deleted',0)->where('perent_id',$teacher->id)->paginate(2);
        $data=[];
        $data['users']=$list;
        return view('student.list',$data);
    }
    // public function serach(Request $request)
    // {
    //     $serach = $request->get('search');
    //     $list=Category::where('name','like',"{$serach}%")->paginate(2);
    //     $data=[];
    //     $data['categories']=$list;
    //     return view('category.list',$data);
    // }
    public function add_student()
    {
        return view('student.add');
    }
    public function save_student(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required',  
            'password' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('add_student'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $teacher = Auth::user();
        $data = $request->all();
        $users = new User();
        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = Hash::make($data['password']);
        $users->perent_id = $teacher->id;
        $users->status = "active";
        $users->role_type = 3 ;
        $users->is_deleted = 0;
        $users->save();
        // $teacher->licence_count = $teacher->licence_count - 1;
        // $teacher->save();

        return redirect('student');
    }
    public function delete_student($id)
    {
        $delete=User::find($id);
        $delete->is_deleted = 1;
        $delete->status = 'deactive';
        $delete->save();
        return redirect('student');
    }
    public function update_student($id)
    {
        $users = User::where('id',$id)->get();
        $data  = [];
        $data['users'] = $users;
        return view('student.update',$data);
    }
    public function updatesave_student($id, Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required',  
            'password' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('update_student'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $users = User::find($id);
        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = Hash::make($data['password']);
        $users->save();
        return redirect('student');
    }
}
