<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use APP\User;
use Auth;
class UserInfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        if($user->role == 'admin'){
            $users = User::get();
        }else{
            $users = User::where('id',$user->id)->get();
        }
        return view('userInfo')->with('users',$users);
    }

   
}
