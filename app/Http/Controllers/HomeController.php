<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user() && Auth::user()->role_type == 'user') {
            return view('welcome.user_welcome');    
        }else{
            return view('welcome');
        }
    }
    public function semple()
    {
       return view('semple');
    }

    // public function index()
    // {
        
    //     return view('welcome');
    // }


   
}
