<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use PDF;
use App\Exam;

class ExamController extends Controller
{
    public function list()
    {
        $user = Auth::user();
        $list = Exam::get();
        $data = [];
        $data['exams'] = $list;
        return view('exam.list',$data);
    }
    public function list_user()
    {
        $user = Auth::user();
        $list = Exam::get();
        $data = [];
        $data['exams'] = $list;
        return view('exam.list-user',$data);
    }
    public function add()
    {
        $user = Auth::user();
        return view('exam.add');
    }
    public function exam_save(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'exam' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('exam_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $user = Auth::user();
        $add = new Exam();
        $add->is_deleted = '0';

        $add->exam = $request->file('exam')->hashName();
        $request->file('exam')
        ->store('exam_pdf',['disk' => 'public']);
        $add->save();

        return redirect('exam/list');
    }

}
