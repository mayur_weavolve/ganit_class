<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

use App\Video;
class VideoController extends Controller
{
    public function list()
    {
        $user = Auth::user();
        $list = Video::where('is_deleted','0')->orderBy('name')->paginate(10);
        $data = [];
        $data['videos'] = $list;
        return view('videos.list',$data);
    }

    public function serach(Request $request)
    {
        $user = Auth::user();
        $serach = $request->get('search');
        $list=Video::where('name','like',"{$serach}%")->where('is_deleted','0')->orderBy('name')->paginate(10);
        $data = [];
        $data['videos'] = $list;
        return view('videos.list',$data);
    }
    public function add()
    {
        $user = Auth::user();
        return view('videos.add');
    }

    public function video_save(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $add = new Video();
        $add->name = $data['name'];
        $add->youtube_link = $data['youtube_link'];
        $add->description = $data['description'];
        $add->status = $data['status'];
        $add->is_deleted = '0';
        $add->save();
        return redirect('video/list');
    }

    public function edit($id)
    {
        $user = Auth::user();
        $list = Video::where('id',$id)->get();
        $data = [];
        $data['videos'] = $list;
        return view('videos.edit',$data);
    }

    public function video_editsave($id,Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $edit = Video::find($id);
        $edit->name = $data['name'];
        $edit->youtube_link = $data['youtube_link'];
        $edit->description = $data['description'];
        $edit->status = $data['status'];
        // $edit->is_deleted = '0';
        $edit->save();
        return redirect('video/list');
    }
    public function video_delete($id)
    {
        $user = Auth::user();
        $delete = Video::find($id);
        $delete->is_deleted = '1';
        $delete->status = 'deactive';
        $delete->save();
        return redirect('video/list');
    }
    public function user_video()
    {
        $user = Auth::user();
        $list = Video::where('is_deleted','0')->get();
        $data = [];
        $data['videos'] = $list;
        return view('videos.user_video',$data);
    }
    public function view_video($id)
    {
        $user = Auth::user();
        $list = Video::where('id',$id)->get();
        $data = [];
        $data['videos'] = $list;
        return view('videos.view',$data);
    }

}
