<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use APP\User;
use Auth;
class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

            $users = User::where('id',$user->id)->get();
            return view('profile')->with('users',$users);
    }

    public function profile(Request $request){

        $data = $request->all();

        $profile = User::find($data['id']);

        $profile->name = $data['name'];
        $profile->address = $data['address'];
        $profile->phone_no = $data['mobile_no'];
        $profile->whatsapp_no = $data['whatsapp_no'];
        $profile->save();

        return redirect('/home');
    }

   
}
