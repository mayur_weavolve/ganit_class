<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'name', 'youtube_link', 'description','status','is_deleted',
    ];
}
