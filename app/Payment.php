<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id','fee','order_id','transaction_id','status','mobile_no',
    ];
}
